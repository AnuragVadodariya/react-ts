//number

let age: number; //or
let arg2: number = 20;

//string

let userName: string = "xyz";

// boolean

let isInstructor: boolean = true;
let isInstructor2: boolean = false;

// null

let hobbies: null;

// non-primitive

let hobbies2: string[];

hobbies2 = ["a", "12"];

let ids: number[] = [12, 3, 2, 45, 65];

// object

let person: {
  name: string | string[];
  age: number;
};

person = {
  name: "abc",
  age: 12,
};

person = {
  name: ["abc", "2939", "12"],
  age: 12,
};

let people: {
  name: string;
  age: number;
} = {
  name: "abc",
  age: 23,
};

//array object

let num: {
  first: number;
  second: number;
}[];

num = [
  { first: 12, second: 43 },
  { first: 44, second: 12 },
];

//type inference

let course = "React";

//course = e303; //get error

// union type

let allCourse: string | number | string[];

allCourse = "React";
allCourse = 3933;
allCourse = ["React", "NodeJs"];

// aliases

type commonObj = {
  id: number | string;
  email: string;
};

let person1: commonObj;

let person2: commonObj;

// let person3: {
//   commonObj;
//   other: string;
// };

type Ans = "Yes" | "No";

const q1: Ans = "Yes";

//const q2: Ans = "yes"; // get Error

// function and type

function printGiven(value) {
  console.log(value);
}

// default any type
// no return means void

// function insertAtBeginning(array, value) {
//   return [value, ...array];
// } // normal fun.

function insertAtBeginning<T>(array: T[], value: T) {
  return [value, ...array];
}

const newArr = insertAtBeginning([2, 3, 4], 1); // auto assign number as type

//newArr[0].splice('') //get Error

const newArr2 = insertAtBeginning(["2", "3", "4"], "1"); // auto assign string as type
